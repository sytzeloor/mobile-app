import DS from 'ember-data';

export default DS.Model.extend({
  username:        DS.attr('string'),
  email:           DS.attr('string'),
  mobile:          DS.attr('string'),
  name:            DS.attr('string'),
  description:     DS.attr('string'),
  avatar:          DS.attr('string'),
  background:      DS.attr('string'),
  url:             DS.attr('string'),
  followersCount:  DS.attr('number'),
  followingsCount: DS.attr('number'),

  videos: DS.hasMany('video', { async: true })
});
