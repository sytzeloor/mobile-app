import DS from 'ember-data';

export default DS.Model.extend({
  thumbnailPath: DS.attr('string'),
  filePath:      DS.attr('string'),
  title:         DS.attr('string'),
  state:         DS.attr('string'),
  description:   DS.attr('string'),
  commentsCount: DS.attr('number'),
  likesCount:    DS.attr('number'),
  viewsCount:    DS.attr('number'),
  createdAt:     DS.attr('date'),

  user: DS.belongsTo('user', { async: true }),
  comments: DS.hasMany('comment', { async: true }),

  relatedVideos: function() {
    var self = this;
    var id = this.get('id');

    this.get('user').then(function(user) {
      user.get('videos').then(function(videos) {
        self.set('relatedVideos', videos.filter(function(item) {
          return item.get('id') !== id;
        }));
      });
    });
  }.property('user')
});
