import DS from 'ember-data';

export default DS.Model.extend({
  comment: DS.attr('string'),
  createdAt: DS.attr('date'),

  user: DS.belongsTo('user', { async: true }),
  video: DS.belongsTo('video', { async: true })
});
