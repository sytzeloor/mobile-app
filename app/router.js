import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource('users', function() {
    this.resource('user', { path: ':user_id' }, function() {
      this.route('edit');
    });
    this.route('new');
  });

  this.resource('videos', function() {
    this.resource('video', { path: ':video_id' }, function() {
      this.route('edit');
    });
    this.route('new');
  });
});

export default Router;
