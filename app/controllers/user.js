import Ember from 'ember';

export default Ember.Controller.extend({
  title: 'Profile',

  topWrapperStyle: function() {
    return ("background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('" + this.model.get('background') + "');").htmlSafe();
  }.property('background')
});
