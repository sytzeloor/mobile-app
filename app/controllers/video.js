import Ember from 'ember';

export default Ember.Controller.extend({
  title: 'Video',
  
  comments: function() {
    var self = this;
    
    this.model.get('comments').then(function(comments) {
      self.set('comments', comments);
    });
  }
});
