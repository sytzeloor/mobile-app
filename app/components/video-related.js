import Ember from 'ember';

export default Ember.Component.extend({
  relatedVideoStyle: function() {
    return ("background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('" + this.video.get('thumbnailPath') + "');").htmlSafe();
  }.property('thumbnailPath')
});
