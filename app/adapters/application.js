import DS from 'ember-data';

export default DS.ActiveModelAdapter.extend({
  host: 'http://localhost:3000',
  namespace: 'api/v1',
  headers: {
    'SESSION_KEY': '356a192b7913b04c54574d18c28d46e6395428ab',
    'SESSION_TOKEN': '157ff34d0d32815789e51871d9037877'
  }
});
